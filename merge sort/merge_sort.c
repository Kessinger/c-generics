#include <stdio.h>
#include "sort_test.h"
#include "merge_sort.h"



int main(){
    intSortTest();
    return 0;
}

void mergesort(int *arr,int start,int end){
    if(start < end){
        int median = (end + start) / 2;
        mergesort(arr,start,median);
        mergesort(arr,median+1,end);
        merge(arr,start,median,end);
    }
}

void merge(int *arr,int start,int median,int end){
    int i = start; int j = median+1;
    int copy[end+1];
    int cIndex = 0;

    while(i <= median && j <= end) {
        if(arr[j] <= arr[i]){
            copy[cIndex++] = arr[j++];
        } else {
            copy[cIndex++] = arr[i++];
        }
    }

    while(i <= median){
        copy[cIndex++] = arr[i++];
    }
    while(j <= end){
        copy[cIndex++] = arr[j++];
    }
    for(int k = 0; k < cIndex; k++){
        arr[start++] = copy[k];
    }
}
