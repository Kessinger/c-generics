#include "generic_insertion_sort.h"

void insertion_sort(void *array, size_t elemSize,size_t arrSize,
                    int (*cmp)(const void *,const void*)){

    void *temp = malloc(elemSize);

    for(size_t i = 0;i < arrSize;i++){
        for(size_t j = i;j > 0;){
            void *elem2 = (char *) array + (j * elemSize);
            void *elem1 = (char *) array + (--j * elemSize);
            if(cmp(elem1,elem2) < 0){
                memcpy(temp,elem2,elemSize);
                memcpy(elem2,elem1,elemSize);
                memcpy(elem1,temp,elemSize);
            }
        }
    }

    free(temp);
}

int cmpInt(const void *elem1,const void * elem2){
    int e1 = *(int *)elem1; // i-1
    int e2 = *(int *)elem2; // i
    if(e2 < e1){
        return -1;
    } else if(e2 > e1){
        return 1;
    } else {
        return 0;
    }
}

int cmpChar(const void *elem1,const void *elem2){
    char e1 = *(char *)elem1; // i-1
    char e2 = *(char *)elem2; // i
    if(e2 < e1){
        return -1;
    } else if(e2 > e1){
        return 1;
    } else {
        return 0;
    }
}

int cmpDouble(const void *elem1,const void *elem2){
    double e1 = *(double *)elem1;
    double e2 = *(double *)elem2;
    if(e2 < e1){
        return -1;
    } else if(e2 > e1){
        return 1;
    } else {
        return 0;
    }
}
// insertion sorting for integers
void iSortInt(int arr[],int size){
    printArr(arr,size,sizeof(int));
    insertion_sort(arr,sizeof(int),size,cmpInt);
    printArr(arr,size,sizeof(int));
}

//insertion sorting for characters
void iSortChar(char arr[],int size){
    printArr(arr,size,sizeof(char));
    insertion_sort(arr,sizeof(char),size,cmpChar);
    printArr(arr,size,sizeof(char));
}

//insertion sorting for floats
void iSortDouble(double arr[],int size){
    printFloat(arr,size);
    insertion_sort(arr,sizeof(double),size,cmpDouble);
    printFloat(arr,size);
}

//Array printer
void printArr(void *arr,int size,int elemSize){
    for(int i = 0;i < size;i++){
        void *elem = (char *) arr + (i * elemSize);
        if(elemSize == sizeof(int)){
            printf("%d ",*(int *)elem);
        } else if(elemSize == sizeof(char)){
            printf("%c ",*(char *)elem);
        }
    }
    printf("\n");
}

void printFloat(double arr[],int size){
    for(int i = 0; i < size; i++){
        printf("%1.1f ",arr[i]);
    }
    printf("\n");
}
