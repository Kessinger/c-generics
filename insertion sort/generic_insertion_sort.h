#ifndef generic_insertion_sort_h
#define generic_insertion_sort_h

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void insertion_sort(void *array, size_t elemSize,size_t arrSize,
                    int (*cmp)(const void *,const void*));

int cmpInt(const void *elem1,const void * elem2);
int cmpChar(const void *elem1,const void *elem2);
int cmpDouble(const void *elem1,const void *elem2);

void iSortInt(int arr[],int size);
void iSortChar(char arr[],int size);
void iSortDouble(double arr[],int size);
void printArr(void *arr,int size,int elemSize);
void printFloat(double arr[],int size);

#endif
