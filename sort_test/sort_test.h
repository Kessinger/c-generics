#ifndef SORT_TEST_H
#define SORT_TEST_H

void initSeed();
void intSortTest();
void doubleSortTest();
int cmpDouble(const void *elem1,const void *elem2);
int cmpInt(const void *elem1,const void * elem2);

#endif // SORT_TEST_H
