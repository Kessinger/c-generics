#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "merge_sort.h"
#include "sort_test.h"

// test size
#define MIN 10
#define MAX 1000000

// int comparator

int cmpInt(const void *elem1,const void * elem2){
    int e1 = *(int *)elem1; // i-1
    int e2 = *(int *)elem2; // i
    if(e2 < e1){
        return -1;
    } else if(e2 > e1){
        return 1;
    } else {
        return 0;
    }
}

// double comparator

int cmpDouble(const void *elem1,const void *elem2){
    double e1 = *(double *)elem1;
    double e2 = *(double *)elem2;
    if(e2 < e1){
        return -1;
    } else if(e2 > e1){
        return 1;
    } else {
        return 0;
    }
}

void initSeed(){
    srand(time(NULL));
}

void intSortTest(){
    initSeed();
    for(size_t i = MIN;i <= MAX;i *=10){
        int arr[i];
        for(size_t j = 0; j < i;j++){
            arr[j] = rand();
        }
	    // sorting the array
        mergesort(arr,0,i);
        // checking if sorted array hold the 
	    // condition i[0] <= i[1] ... <= i[n].
        for(size_t j = 1;j < i;j++){
            int *e1 = &arr[j-1];
            int *e2 = &arr[j];
	    assert(cmpInt(e2,e1) <= 0);
        }
        printf("INT TEST : %7d\tPASSED\n",i);
    }
    printf("\n");
}

void doubleSortTest(){
    initSeed();
    for(int i = MIN; i <= MAX; i *= 10){
        double arr[i];
        for(int j = 0 ; j < i;j++){
            arr[j] = (double)(rand() % 100) + 1.0;
        }
        // perform sort
        //insertion_sort(arr,sizeof (double),i,cmpDouble);
        for(int j = 1; j < i;j++){
            double *e1 = &arr[j-1];
            double *e2 = &arr[j];
            assert(cmpDouble(e2,e1) <= 0);
        }
        printf("Double Test : %5d\tPASSED\n",i);
    }
    printf("\n");
}
